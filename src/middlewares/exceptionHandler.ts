import logger from '@/logger';
import { NextFunction, Request, Response } from 'express';

export const exceptionHandler = (
  error: Error,
  _: Request,
  rs: Response,
  //  Потому что сигнатура у функции должна быть такая http://expressjs.com/en/4x/api.html#app.use
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  __: NextFunction
) => {
  logger.error(error);
  rs.status(500).send({
    status: 'failed',
    details: process.env.NODE_ENV !== 'production' && error.message,
  });
};
