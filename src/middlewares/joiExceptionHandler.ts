import { ExpressJoiError } from 'express-joi-validation';
import express from 'express';

export const joiExceptionHandler = (
  err: any | ExpressJoiError,
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) => {
  // ContainerTypes is an enum exported by this module. It contains strings
  // such as "body", "headers", "query"...
  if (err?.error?.isJoi) {
    const e: ExpressJoiError = err;
    // e.g "you submitted a bad query paramater"
    res.status(400).send({
      status: 'failed',
      details: e.error?.details.map(({ message, path }) => ({ path, message })),
    });
  } else {
    next(err);
  }
};
