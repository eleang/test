import jwt from 'jsonwebtoken';
import omitBy from 'lodash/omitBy';
import { NextFunction, Request, Response } from 'express';

const { JWT_SECRET } = process.env;

export default async (req: Request, res: Response, next: NextFunction) => {
  try {
    const token = req.header('Authorization');
    res.locals.user = jwt.verify(token as string, JWT_SECRET as string) as {
      id: number;
    };

    next();
  } catch (error) {
    res.status(401).json(
      omitBy(
        {
          errors: 1,
          message:
            'При проверке подлинности произошла ошибка, пожалуйста войдите ещё раз',
          details:
            process.env.NODE_ENV !== 'production' &&
            (error?.toJSON ? error.toJSON() : JSON.stringify(error)),
        },
        (v) => !v
      )
    );
  }
};
