/* eslint-disable import/first */ // Потому что надо сначала сконфигурировать переменные, а потом импортировать
/* eslint-disable import/order */ // Потому что надо сначала сконфигурировать переменные, а потом импортировать
const parsedResult = require('dotenv').config();

if (parsedResult.error) {
  throw parsedResult.error;
}

import http, { Server } from 'http';
import app from './app';
import logger from './logger';
import socket from './socket';

const { PORT = 4000 } = process.env;

const server: Server = http.createServer(app);
socket(server);
server.listen(PORT, () =>
  logger.info(`Server ready on http://localhost:${PORT}`)
);
