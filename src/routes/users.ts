import { models } from '@/models';
import { Router } from 'express';

const router = Router();
router.get('/', async (_, rs) => {
  rs.send(await models.ChatMember.findAll());
});

router.get('/:memberId', async (rq, rs) => {
  rs.send(await models.ChatMember.findByPk(rq.params.memberId));
});

router.delete('/:memberId', async (rq, rs) => {
  await models.ChatMember.destroy({ where: { id: rq.params.memberId } });
  rs.end();
});

router.post('/', async (rq, rs) => {
  await models.ChatMember.create(rq.body);
  rs.end();
});

router.patch('/:memberId', async (rq, rs) => {
  await models.ChatMember.update(rq.body, {
    where: { id: rq.params.memberId },
  });
  rs.end();
});

export default router;
