import supertest from 'supertest';
import app from '@/app';
import jwt from 'jsonwebtoken';
import { models } from '@/models';

const request = supertest(app);
// @ts-ignore
const token = jwt.sign({ id: 1 }, process.env.JWT_SECRET);

describe('user-service', () => {
  it('create user', async () => {
    const chat = await models.Chat.create({ id: 2 });
    await request
      .post('/users/')
      .send({ userId: 2, chatId: 2 })
      .set('Authorization', token);
    const members = await models.ChatMember.findAll({
      where: { userId: 2 },
      raw: true,
    });
    expect(members).toHaveLength(1);

    expect(members[0]).toEqual({
      userId: 2,
      chatId: 2,
      createdAt: expect.any(Date),
      updatedAt: expect.any(Date),
      id: expect.any(Number),
    });
    await chat.destroy();
  });
  it('get all users', async () => {
    await models.Chat.destroy({
      where: { id: Array.from({ length: 10 }, (_, i) => i + 1) },
      cascade: true,
    });
    await models.ChatMember.bulkCreate(
      Array.from(
        {
          length: 10,
        },
        (_, i) => ({ id: i + 1, userId: i + 1, Chat: { id: i + 1 } })
      ),
      { include: [models.Chat] }
    );
    const { body: data } = await request
      .get('/users')
      .set('Authorization', token);

    expect(data).toEqual(
      (await models.ChatMember.findAll({ raw: true })).map((item) => ({
        ...item,
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
      }))
    );
    await models.Chat.destroy({
      where: { id: Array.from({ length: 10 }, (_, i) => i + 1) },
      cascade: true,
    });
  });
  it('update user', async () => {
    await models.ChatMember.destroy({ where: { id: 3, userId: 3 } });
    await models.Chat.destroy({ where: { id: 3 }, cascade: true });
    await models.ChatMember.create(
      {
        id: 3,
        userId: 4,
        Chat: {
          id: 3,
        },
      },
      { include: [models.Chat] }
    );
    await request
      .patch(`/users/${3}`)
      .send({ userId: 2 })
      .set('Authorization', token)
      .expect(200);

    const members = await models.ChatMember.findAll({
      where: { id: 3 },
      raw: true,
    });

    expect(members).toEqual([
      {
        id: 3,
        chatId: 3,
        userId: 2,
        createdAt: expect.any(Date),
        updatedAt: expect.any(Date),
      },
    ]);
    await models.Chat.destroy({ where: { id: 3 }, cascade: true });
  });

  it('remove user by id', async () => {
    await models.ChatMember.destroy({ where: { id: 4 } });
    await models.Chat.destroy({ where: { id: 4 } });
    await models.ChatMember.create(
      {
        id: 4,
        userId: 4,
        Chat: { id: 4 },
      },
      { include: [models.Chat] }
    );
    await request.delete(`/users/${4}`).set('Authorization', token).expect(200);
    const members = await models.ChatMember.findAll({
      where: { id: 4 },
      raw: true,
    });
    expect(members).toHaveLength(0);
    await models.Chat.destroy({ where: { id: 4 } });
  });
});
