import app from '@/app';
import { models } from '@/models';
import supertest from 'supertest';
import jwt from 'jsonwebtoken';

const request = supertest(app);

// @ts-ignore
const token = jwt.sign({ id: 1 }, process.env.JWT_SECRET);

describe('rest chat', () => {
  it('get all', async () => {
    await models.Chat.destroy({
      truncate: true,
      logging: console.log,
      cascade: true,
    });
    await models.Chat.create({
      id: 1,
    });
    const { body: chats } = await request
      .get('/chats')
      .set('Authorization', token)
      .expect(200);

    expect(chats).toEqual([
      {
        id: 1,
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
        active: true,
      },
    ]);
    await models.Chat.destroy({
      where: {
        id: 1,
      },
    });
  });
  it('create chat', async () => {
    await models.Chat.destroy({
      where: {
        id: 2,
      },
    });
    const { body: chat } = await request
      .post('/chats')
      .send({ id: 2 })
      .set('Authorization', token)
      .expect(200);
    expect(chat).toEqual({
      id: 2,
      createdAt: expect.toBeDate(),
      updatedAt: expect.toBeDate(),
      active: true,
    });
    await models.Chat.destroy({
      where: {
        id: 2,
      },
    });
  });
});
