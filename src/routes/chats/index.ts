import { models } from '@/models';
import Router from 'express';
import chatRouter from './_id';
import { messageRouter } from './_id/messages';

const router = Router();
router.use(chatRouter);
router.use(messageRouter);

router.get('/', async (_, rs) => {
  rs.send(await models.Chat.findAll({ raw: true }));
});

router.post('/', async (rq, rs) => {
  rs.send(await models.Chat.create(rq.body, { raw: true }));
});

export default router;
