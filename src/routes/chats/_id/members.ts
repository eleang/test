import { models } from '@/models';
import Router from 'express';

const router = Router();

router.get('/:chatId/members', async (rq, rs) => {
  rs.send(
    await models.ChatMember.findAll({
      where: { chatId: rq.params.chatId },
    })
  );
});

router.post('/:chatId/members', async (rq, rs) => {
  rs.send(
    await models.ChatMember.create(
      {
        ...rq.body,
        chatId: +rq.params.chatId,
      },
      { raw: true }
    )
  );
});

router.patch('/:chatId/members/:userId', async (rq, rs) => {
  const { chatId, userId } = rq.params;
  const [count, [instance]] = await models.ChatMember.update(rq.body, {
    where: { userId, chatId },
    returning: true,
  });
  if (!count)
    rs.status(403).send({
      status: 'failed',
      message: 'Участник не найден',
    });

  rs.send(instance.toJSON());
});

router.delete('/:chatId/members/:userId', async (rq, rs) => {
  const { chatId, userId } = rq.params;
  await models.ChatMember.destroy({
    where: {
      chatId,
      userId,
    },
  });
  rs.end();
});

export default router;
