import app from '@/app';
import { models } from '@/models';
import supertest from 'supertest';
import jwt from 'jsonwebtoken';

const request = supertest(app);

// @ts-ignore
const token = jwt.sign({ id: 1 }, process.env.JWT_SECRET);

test('should get chat by id', async () => {
  await models.Chat.destroy({ where: { id: 3 } });
  await models.Chat.create({
    id: 3,
  });
  const { body: chat } = await request
    .get(`/chats/${3}`)
    .set('Authorization', token)
    .expect(200);
  expect(chat).toEqual({
    id: 3,
    createdAt: expect.toBeDate(),
    updatedAt: expect.toBeDate(),
    active: true,
  });
  models.Chat.destroy({ where: { id: 3 } });
});
test('should delete chat by id', async () => {
  await models.Chat.destroy({ where: { id: 4 } });
  await models.Chat.create({
    id: 4,
  });
  await request.delete(`/chats/${4}`).set('Authorization', token).expect(200);

  expect(await models.Chat.findByPk(4)).toBeNull();
});

it('should update chat by id', async () => {
  await models.Chat.destroy({ where: { id: 5 } });
  await models.Chat.create({
    id: 5,
  });
  const { body: chat } = await request
    .patch(`/chats/${5}`)
    .set('Authorization', token)
    .send({ active: false })
    .expect(200);

  expect(await models.Chat.findByPk(5, { raw: true })).toEqual({
    id: 5,
    createdAt: expect.toBeDate(),
    updatedAt: expect.toBeDate(),
    active: false,
  });
  expect(chat).toEqual({
    id: 5,
    createdAt: expect.toBeDate(),
    updatedAt: expect.toBeDate(),
    active: false,
  });
});
