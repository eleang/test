import app from '@/app';
import { models } from '@/models';
import supertest from 'supertest';
import jwt from 'jsonwebtoken';

const { JWT_SECRET } = process.env as { JWT_SECRET: string };
const request = supertest(app);
const token = jwt.sign({ id: 1 }, JWT_SECRET);
describe('chats/:chatId/messages', () => {
  it('should get all messages', async () => {
    await models.Chat.destroy({ truncate: true, cascade: true });
    await models.ChatMember.destroy({ truncate: true, cascade: true });
    const chat = await models.Chat.create({
      id: 1,
    });
    // @ts-ignore
    await chat.createMember({
      id: 1,
      userId: 1,
    });
    await models.Message.bulkCreate(
      Array.from({ length: 20 }, (_, i) => ({
        id: i + 1,
        content: `message#${i + 1}`,
        chatId: 1,
        authorId: 1,
      }))
    );
    const response = await request
      .get(`/chats/${1}/messages`)
      .set('Authorization', token)
      .expect(200);

    const { body: messages } = response;

    expect(messages).toEqual(
      Array.from({ length: 20 }, (_, i) => ({
        id: i + 1,
        content: `message#${i + 1}`,
        chatId: 1,
        authorId: 1,
        files: null,
        replyTo: null,
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
      }))
    );
  });

  it('should get messages by content', async () => {
    await models.Chat.destroy({ truncate: true, cascade: true });
    await models.ChatMember.destroy({ truncate: true, cascade: true });
    const chat = await models.Chat.create({
      id: 1,
    });
    // @ts-ignore
    await chat.createMember({
      id: 1,
      userId: 1,
    });
    await models.Message.bulkCreate(
      Array.from({ length: 20 }, (_, i) => ({
        id: i + 1,
        content: `message#${i + 1}`,
        chatId: 1,
        authorId: 1,
      }))
    );
    const response = await request
      .get(`/chats/${1}/messages`)
      .set('Authorization', token)
      .query({
        filter: {
          content: 'message#1',
        },
      });

    const { body: messages } = response;

    expect(messages).toEqual([
      {
        id: 1,
        content: `message#1`,
        chatId: 1,
        authorId: 1,
        files: null,
        replyTo: null,
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
      },
    ]);
  });

  it('should get messages by userId', async () => {
    await models.Chat.destroy({ truncate: true, cascade: true });
    await models.ChatMember.destroy({ truncate: true, cascade: true });
    const chat = await models.Chat.create({
      id: 1,
    });
    // @ts-ignore
    await chat.createMember({
      id: 1,
      userId: 1,
    });
    // @ts-ignore
    await chat.createMember({
      id: 2,
      userId: 2,
    });
    await models.Message.bulkCreate(
      Array.from({ length: 20 }, (_, i) => ({
        id: i + 1,
        content: `message#${i + 1}`,
        chatId: 1,
        authorId: 1,
      }))
    );
    await models.Message.bulkCreate(
      Array.from({ length: 20 }, (_, i) => ({
        id: i + 21,
        content: `message#${i + 21}`,
        chatId: 1,
        authorId: 2,
      }))
    );
    const response = await request
      .get(`/chats/${1}/messages`)
      .set('Authorization', token)
      .query({
        filter: {
          authorId: 2,
        },
      });

    const { body: messages } = response;

    expect(messages).toEqual(
      Array.from({ length: 20 }, (_, i) => ({
        id: i + 21,
        content: `message#${i + 21}`,
        chatId: 1,
        authorId: 2,
        files: null,
        replyTo: null,
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
      }))
    );
  });

  it('should get messages by files', async () => {
    await models.Chat.destroy({ truncate: true, cascade: true });
    await models.ChatMember.destroy({ truncate: true, cascade: true });
    const chat = await models.Chat.create({
      id: 1,
    });
    // @ts-ignore
    await chat.createMember({
      id: 1,
      userId: 1,
    });

    await models.Message.bulkCreate(
      Array.from({ length: 20 }, (_, i) => ({
        id: i + 1,
        content: `message#${i + 1}`,
        chatId: 1,
        authorId: 1,
        files: [1, i],
      }))
    );

    const response = await request
      .get(`/chats/${1}/messages`)
      .set('Authorization', token)
      .query({
        filter: {
          fileId: [1, 2],
        },
      });

    const { body: messages } = response;

    expect(messages).toEqual(
      Array.from({ length: 10 }, (_, i) => ({
        id: i + 1,
        content: `message#${i + 1}`,
        chatId: 1,
        authorId: 1,
        files: [1],
        replyTo: null,
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
      }))
    );
  });
});
