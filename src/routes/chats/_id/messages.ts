import * as Joi from 'joi';
import { models } from '@/models';
import { Router } from 'express';
import {
  ContainerTypes,
  // Use this as a replacement for express.Request
  ValidatedRequest,
  // Extend from this to define a valid schema type/interface
  ValidatedRequestSchema,
  // Creates a validator that generates middlewares
  createValidator,
} from 'express-joi-validation';
import { isEmpty, isObject, omitBy } from 'lodash';

const validator = createValidator({
  passError: true,
});

const messageRouter = Router();

const idJoiValidator = () =>
  Joi.alternatives().try(
    Joi.number().integer().optional(),
    Joi.array().items(Joi.number().integer()).optional()
  );

const getAllQuerySchema = Joi.object({
  filter: Joi.object({
    content: Joi.string().optional(),
    fileId: idJoiValidator().optional(),
    authorId: idJoiValidator().optional(),
  }).optional(),
});

const getAllMessagesParamsSchema = Joi.object({
  chatId: Joi.number().integer().greater(0).required(),
});

interface GetAllMessagesQuerySchema extends ValidatedRequestSchema {
  [ContainerTypes.Query]: {
    filter?: {
      content?: string;
      fileId?: string;
      authorId?: string | string[];
    };
  };
  [ContainerTypes.Params]: { chatId: string };
}
// @ts-ignore
const recursiveOmitBy: typeof omitBy = (coreValue, func) => {
  if (isObject(coreValue)) {
    return omitBy(coreValue, (value) => {
      const r = recursiveOmitBy(value, func);
      if (isObject(r)) return isEmpty(r);
      return r;
    });
  }
  return func(coreValue);
};

messageRouter.get(
  '/:chatId/messages',
  validator.query(getAllQuerySchema),
  validator.params(getAllMessagesParamsSchema),
  async (rq: ValidatedRequest<GetAllMessagesQuerySchema>, rs) => {
    const where = recursiveOmitBy(
      {
        chatId: rq.params.chatId,
        content: rq.query.filter?.content,
        authorId: rq.query.filter?.authorId,
        files: {
          $contained: [rq.query.filter?.fileId].flat(Infinity),
        },
      },
      (v) => !v
    );
    rs.send(
      await models.Message.findAll({
        // @ts-ignore
        where,
        raw: true,
        logging: console.log,
      })
    );
  }
);

export { messageRouter };
