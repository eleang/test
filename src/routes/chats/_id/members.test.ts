import app from '@/app';
import { models } from '@/models';
import supertest from 'supertest';
import jwt from 'jsonwebtoken';

const request = supertest(app);

// @ts-ignore
const token = jwt.sign({ id: 1 }, process.env.JWT_SECRET);
describe('chat members', () => {
  it('should get members by chat id ', async () => {
    await models.Chat.destroy({
      where: { id: 10 },
    });
    await models.ChatMember.destroy({ where: { id: [10, 20] } });

    await models.Chat.create(
      {
        id: 10,
        members: [
          {
            id: 10,
            userId: 10,
          },
          {
            id: 20,
            userId: 20,
          },
        ],
      },
      {
        include: ['members'],
      }
    );

    const { body: members } = await request
      .get(`/chats/${10}/members`)
      .set('Authorization', token)
      .expect(200);

    const chatMemberTemplate = {
      chatId: 10,
      createdAt: expect.toBeDate(),
      updatedAt: expect.toBeDate(),
    };
    expect(members).toEqual([
      {
        id: 10,
        userId: 10,
        ...chatMemberTemplate,
      },
      {
        id: 20,
        userId: 20,
        ...chatMemberTemplate,
      },
    ]);
    await models.Chat.destroy({
      where: { id: 10 },
      cascade: true,
    });
    await models.ChatMember.destroy({ where: { id: [10, 20] } });
  });

  it('should create chat member', async () => {
    await models.Chat.destroy({
      where: { id: 30 },
      cascade: true,
    });
    await models.ChatMember.destroy({
      where: {
        id: 30,
      },
      cascade: true,
    });
    await models.Chat.create(
      {
        id: 30,
      },
      { raw: true }
    );
    const { body: member } = await request
      .post(`/chats/${30}/members`)
      .send({
        userId: 30,
      })
      .set('Authorization', token)
      .expect(200);

    const memberResult = {
      id: expect.any(Number),
      userId: 30,
      chatId: 30,
      createdAt: expect.toBeDate(),
      updatedAt: expect.toBeDate(),
    };
    expect(member).toEqual(memberResult);

    // @ts-ignore
    expect(
      (
        await models.ChatMember.findOne({
          where: { chatId: 30, userId: 30 },
        })
      )?.toJSON()
    ).toEqual(memberResult);

    await models.Chat.destroy({ where: { id: 30 }, cascade: true });
  });

  it('should update chat member', async () => {
    await models.Chat.destroy({
      where: { id: 40 },
      cascade: true,
    });
    await models.ChatMember.destroy({
      where: {
        id: 40,
      },
      cascade: true,
    });

    await models.Chat.create(
      {
        id: 40,
        members: [
          {
            id: 40,
            userId: 40,
          },
        ],
      },
      { raw: true, include: ['members'] }
    );

    const { body: member } = await request
      .patch(`/chats/${40}/members/${40}`)
      .send({
        userId: 340,
      })
      .set('Authorization', token)
      .expect(200);

    const memberResult = {
      userId: 340,
      chatId: 40,
      id: 40,
      createdAt: expect.toBeDate(),
      updatedAt: expect.toBeDate(),
    };
    expect(member).toEqual(memberResult);

    // @ts-ignore
    expect(
      (
        await models.ChatMember.findOne({
          where: { chatId: 40, userId: 340 },
        })
      )?.toJSON()
    ).toEqual(memberResult);

    await models.Chat.destroy({
      where: { id: 40 },
      cascade: true,
    });
  });

  it('should remove chat member', async () => {
    await models.Chat.destroy({
      where: { id: 50 },
      cascade: true,
    });
    await models.ChatMember.destroy({
      where: {
        id: 50,
      },
      cascade: true,
    });

    await models.Chat.create(
      {
        id: 50,
        members: [
          {
            id: 50,
            userId: 50,
          },
        ],
      },
      { raw: true, include: ['members'] }
    );

    await request
      .delete(`/chats/${50}/members/${50}`)
      .set('Authorization', token)
      .expect(200);

    // @ts-ignore
    expect(
      await models.ChatMember.findOne({
        where: { chatId: 50 },
      })
    ).toBeNull();

    await models.Chat.destroy({
      where: { id: 50 },
      cascade: true,
    });
    await models.ChatMember.destroy({ where: { id: 50 } });
  });
});
