import { models } from '@/models';
import Router from 'express';
import chatMembersRouter from './members';

const router = Router();

router.use(chatMembersRouter);

router.get('/:chatId', async (rq, rs) => {
  rs.send(await models.Chat.findByPk(+rq.params.chatId, { raw: true }));
});

router.delete('/:chatId', async (rq, rs) => {
  await models.Chat.destroy({ where: { id: rq.params.chatId } });
  rs.end();
});

router.patch('/:chatId', async (rq, rs) => {
  const chat = await models.Chat.findByPk(rq.params.chatId);
  if (!chat) {
    rs.status(401).send({
      status: 'failed',
      details: 'Чат не найден',
    });
  }
  await chat?.update(rq.body);
  rs.send(chat?.toJSON());
});

export default router;
