import { Sequelize, Model, DataTypes, ModelCtor } from 'sequelize';

export default (sequelize: Sequelize) => {
  class ReadedMessage extends Model {
    static associate(models: { [name: string]: ModelCtor<any> }) {
      this.belongsTo(models.ChatMember, {
        foreignKey: 'chatMemberId',
        as: 'chatMember',
      });
      this.belongsTo(models.Message, {
        foreignKey: 'messageId',
        as: 'message',
      });
    }
  }
  ReadedMessage.init(
    {
      messageId: { type: DataTypes.INTEGER, allowNull: false },
      chatMemberId: { type: DataTypes.INTEGER, allowNull: false },
    },
    { sequelize }
  );
  return ReadedMessage;
};
