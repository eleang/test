import { Sequelize, DataTypes, Model, ModelCtor } from 'sequelize';

export default (sequelize: Sequelize) => {
  class Message extends Model {
    content!: string;

    files!: number[];

    static associate(models: { [name: string]: ModelCtor<any> }) {
      Message.belongsTo(models.Chat, { foreignKey: 'chatId', as: 'chat' });
      Message.belongsTo(models.Message, {
        foreignKey: 'replyTo',
        as: 'replyTarget',
      });
      Message.hasMany(models.Message, { foreignKey: 'replyTo', as: 'reply' });
      Message.belongsTo(models.ChatMember, {
        foreignKey: 'authorId',
        as: 'author',
      });
      Message.belongsToMany(models.ChatMember, {
        foreignKey: 'messageId',
        as: 'membersReaded',
        through: models.ReadedMessage,
      });
    }
  }
  Message.init(
    {
      content: DataTypes.STRING,
      authorId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      chatId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      files: DataTypes.JSONB,
    },
    {
      sequelize,
      paranoid: true,
      defaultScope: {
        attributes: {
          exclude: ['deletedAt'],
        },
      },
    }
  );
  return Message;
};
