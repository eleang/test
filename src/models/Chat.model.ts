import { Sequelize, Model, DataTypes, ModelCtor } from 'sequelize';

export default (sequelize: Sequelize) => {
  class Chat extends Model {
    // name!: string;

    active!: boolean;

    static associate(models: { [name: string]: ModelCtor<any> }) {
      Chat.hasMany(models.Message, { foreignKey: 'chatId', as: 'messages' });
      Chat.hasMany(models.ChatMember, {
        foreignKey: 'chatId',
        as: 'members',
      });
    }
  }

  Chat.init(
    {
      // name: DataTypes.STRING,
      active: { type: DataTypes.BOOLEAN, defaultValue: true },
    },
    { sequelize }
  );

  return Chat;
};
