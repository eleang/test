import { Sequelize, Model, DataTypes, ModelCtor, Op } from 'sequelize';

const env = process.env.NODE_ENV || 'development';
const config = require(`../config/config.json`)[env];

const sequelize: Sequelize = config.use_env_variable
  ? // @ts-ignore
    new Sequelize(process.env[config.use_env_variable], config)
  : new Sequelize(config.database, config.username, config.password, {
      ...config,
      operatorsAliases: {
        $contains: Op.contains,
        $contained: Op.contained,
        $in: Op.in,
      },
    });

const models: {
  [key: string]: ModelCtor<Model>;
} = {};

[
  require('./Chat.model'),
  require('./ChatMember.model'),
  require('./Message.model'),
  require('./ReadedMessage.model'),
].forEach(({ default: mdl }) => {
  const model = mdl(sequelize, DataTypes);
  models[model.name] = model;
});

// sequelize.sync({ force: true }).then(() => console.log('finish'));
// @ts-ignore Потому что ради одного раза не буду расширять тайпинги
Object.values(models).forEach((mdl) => mdl?.associate?.(models));

export { models, sequelize, Sequelize };
