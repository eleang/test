import { Sequelize, Model, DataTypes, ModelCtor } from 'sequelize';

export default (sequelize: Sequelize) => {
  class ChatMember extends Model {
    static associate(models: { [name: string]: ModelCtor<any> }) {
      this.belongsTo(models.Chat, { foreignKey: 'chatId' });
      this.belongsToMany(models.Message, {
        through: models.ReadedMessage,
        foreignKey: 'chatMemberId',
        as: 'readedMessages',
      });
      this.hasMany(models.Message, {
        foreignKey: 'authorId',
        as: 'messages',
      });
    }
  }
  ChatMember.init(
    {
      chatId: { type: DataTypes.INTEGER, allowNull: false },
      userId: { type: DataTypes.INTEGER, allowNull: false },
    },
    { sequelize }
  );
  return ChatMember;
};
