import express from 'express';
import logger from '@/logger';
import 'express-async-errors';
import { models } from './models';
import usersRouter from './routes/users';
import chatsRouter from './routes/chats';
import jwtAuth from './middlewares/jwtAuth';
import { exceptionHandler } from './middlewares/exceptionHandler';
import { joiExceptionHandler } from './middlewares/joiExceptionHandler';

const app = express();
app.use(require('helmet')());

app.locals = {
  logger,
  models,
};
app.use(express.json());
app.use(jwtAuth);
app.use('/users', usersRouter);
app.use('/chats', chatsRouter);

app.use(joiExceptionHandler);
app.use(exceptionHandler);

export default app;
