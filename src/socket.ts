import { Server } from 'http';
import socketIo, { Socket } from 'socket.io';
import jwt from 'jsonwebtoken';
import chatSocketRouter from './socket-routes/chat';
import { models } from './models';
import logger from './logger';

const socketLogger = logger.child({ component: 'socket-service' });

async function authenithicate(token: any) {
  return jwt.verify(token as string, process.env.JWT_SECRET as string) as {
    id: number;
  };
}
export default (http: Server) => {
  // @ts-ignore
  const io: Socket = socketIo(http);
  // @ts-ignore
  io.use(async (socket, next) => {
    try {
      const { token } = socket.handshake.auth;
      // @ts-ignore
      // eslint-disable-next-line no-cond-assign
      if ((socket.user = await authenithicate(token))) {
        return next();
      }
    } catch (error) {
      next(error);
    }
  });

  models.Chat.addHook('afterDestroy', ({ id }: any) => {
    io.to(id).emit('chat-removed');
  });

  models.Chat.addHook('afterCreate', async (chat: any) => {
    const members = await chat.getMembers();
    let payloadIo = io;
    // @ts-ignore
    members.forEach((id) => {
      payloadIo = payloadIo.to(`user-${id}`);
    });

    payloadIo.emit('chat-created', chat.id);
  });

  io.on('connection', (socket) => {
    socketLogger.info('user connected', socket.user.id);
    chatSocketRouter(socket);
    socket.on('disconnect', () => {
      socketLogger.info('user disconnected', socket.user.id);
    });
  });

  return io;
};
