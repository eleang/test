import coreLogger from '@/logger';
import { models } from '@/models';

import { Socket } from 'socket.io';

const logger = coreLogger.child({ component: 'socket-routes/chat' });

export default async (socket: Socket) => {
  try {
    // @ts-ignore
    const { user } = socket;
    const chatIds = (
      await models.Chat.findAll({
        include: [
          {
            as: 'members',
            model: models.ChatMember,
            where: { userId: user.id },
            attributes: [],
            required: true,
          },
        ],
      })
    )?.map?.(({ id }: any) => id);
    socket.join(
      // @ts-ignore
      chatIds
    );

    socket.join(`user-${user.id}`);

    socket.on('send-message', async (message, cb) => {
      try {
        const { chatId } = message;
        socket.to(chatId).emit('send-message', message);
        const author = await models.ChatMember.findOne({
          where: { userId: user.id, chatId },
          rejectOnEmpty: true,
        });
        await models.Message.create(
          {
            ...message,
            // @ts-ignore
            authorId: author.id,
            chatId,
          },
          { include: ['author'] }
        );
        cb({ status: 'successed' });
      } catch (error) {
        logger.error(error);
        cb({
          status: 'failed',
          error: error.message || error.toString(),
        });
      }
    });

    socket.on('typing', ({ chatId, memberId }) => {
      socket.to(chatId).emit('typing', { chatId, memberId });
    });

    socket.on('reading-message', async (messageId, cb) => {
      try {
        const message = await models.Message.findByPk(messageId, {
          include: ['chat'],
          rejectOnEmpty: true,
        });
        const { id: chatMemberId } = await models.ChatMember.findOne({
          where: {
            // @ts-ignore
            userId: socket.user.id,
            // @ts-ignore
            chatId: message.chat.id,
          },
          rejectOnEmpty: true,
          raw: true,
        });
        await models.ReadedMessage.create({
          messageId,
          chatMemberId,
        });
        // @ts-ignore
        // TODO: Интерфейс скорее всего поменяем при написании фронта
        socket.to(message.chat.id).emit('reading-message', messageId);
        cb({
          status: 'successed',
        });
      } catch (error) {
        logger.error(error);
        cb({ status: 'failed', error: error.message || error.toString() });
      }
    });

    // @ts-ignore
    socket.on('has-ready', (cb) => cb(socket.ready));
    // @ts-ignore
    socket.ready = true;
    socket.emit('ready');
  } catch (error) {
    logger.error(error);
    socket.disconnect(true);
  }
};
