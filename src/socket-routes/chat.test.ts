import { Model } from 'sequelize/types';
import io, { Socket } from 'socket.io-client';
import jwt from 'jsonwebtoken';
import { models } from '@/models';

const { JWT_SECRET } = process.env as typeof process.env & {
  JWT_SECRET: string;
};

async function waitSocketToReady(socket: typeof Socket) {
  await Promise.all([
    // Ждём пока он оживёт или не оживёт
    new Promise<void>((resolve, reject) => {
      // Проверяем, потому что возможно он уже сделал конект
      socket.connected && resolve();
      socket.on('connect', resolve);
      socket.on('connect_error', reject);
      socket.on('disconnect', reject);
      // Просто костыль, потому что асинхронность
      setTimeout(() => {
        socket.connected ? resolve() : reject('more than 500ms connect');
      }, 2000);
    }),

    // Ждём пока сервак всё проинициализирует
    new Promise<void>((resolve, reject) => {
      // Проверяем, потому что возможно уже всё красиво
      socket.emit('has-ready', (ready: boolean) => {
        // @ts-ignore
        ready && resolve();
      });
      socket.once('ready', resolve);
      setTimeout(() => {
        socket.emit('has-ready', (ready: boolean) => {
          // @ts-ignore
          ready ? resolve() : reject();
        });
      }, 500);
    }),
  ]);
}

const socketUrl = process.env.SOCKET_URL ?? 'ws://localhost:4000';
describe('chat sockets', () => {
  it('connect user', async () => {
    await models.ChatMember.destroy({ where: { id: [1] } });
    const firstMember = await models.ChatMember.create(
      {
        id: 1,
        userId: 1,
        Chat: {
          id: 1,
        },
      },
      { include: [models.Chat] }
    );
    const socket = io(socketUrl, {
      // @ts-ignore
      auth: {
        token: jwt.sign({ id: 1 }, JWT_SECRET),
      },
    });
    await waitSocketToReady(socket);
    socket.close();
    await firstMember.destroy();
  });

  it('should create record in db after send message', async () => {
    await models.Chat.destroy({ where: { id: 110 }, cascade: true });
    const member = await models.ChatMember.create(
      { id: 110, userId: 1, Chat: { id: 110 } },
      { include: [models.Chat] }
    );
    const message = {
      chatId: 110,
      content: 'Hello',
    };
    // Инициализируем сокет
    const socket = io(socketUrl, {
      // @ts-ignore
      auth: {
        token: jwt.sign({ id: 1 }, JWT_SECRET),
      },
    });

    // Ждём пока он оживёт или не оживёт
    await waitSocketToReady(socket);

    // Отправляем сообщение и ждём пока сервак скажет что всё ок
    await new Promise<void>((res) =>
      socket.emit('send-message', message, res)
    ).then((response) => expect(response).toEqual({ status: 'successed' }));

    // @ts-ignore
    // Выбираем все сообщения чата
    const messages = await member.Chat.getMessages({ raw: true });

    // Сообщение должно быть с тем самым контентом
    expect(messages).toEqual([
      {
        ...message,
        // @ts-ignore
        authorId: member.id,
        // @ts-ignore
        chatId: member.Chat.id,
        createdAt: expect.any(Date),
        updatedAt: expect.any(Date),
        replyTo: null,
        id: expect.any(Number),
      },
    ]);

    socket.close();
    await models.Chat.destroy({ where: { id: 110 }, cascade: true });
  });

  it('second member should receive message', async () => {
    await models.Chat.destroy({ where: { id: 10 }, cascade: true });
    await models.ChatMember.destroy({ where: { id: [10, 20] } });
    await models.Chat.create(
      {
        id: 10,
        members: [
          {
            id: 10,
            userId: 1,
          },
          {
            id: 20,
            userId: 2,
          },
        ],
      },
      { include: ['members'] }
    );
    const message = {
      chatId: 10,
      content: 'Hello',
    };
    // Инициализируем сокет
    const firstMemberSocket = io(socketUrl, {
      // @ts-ignore
      auth: {
        token: jwt.sign({ id: 1 }, JWT_SECRET),
      },
    });
    const secondMemberSocket = io(socketUrl, {
      // @ts-ignore
      auth: {
        token: jwt.sign({ id: 2 }, JWT_SECRET),
      },
    });

    await Promise.all([
      waitSocketToReady(firstMemberSocket),
      waitSocketToReady(secondMemberSocket),
    ]);

    const mockFuncCalledWithMessage = jest.fn();

    // Вешаем обработчик за второго участника и при получении присваиваем значения для последующей сверки
    secondMemberSocket.on('send-message', mockFuncCalledWithMessage);

    // Отправляем сообщение и ждём пока сервак скажет что всё ок
    await new Promise<void>((res) =>
      firstMemberSocket.emit('send-message', message, res)
    );

    expect(mockFuncCalledWithMessage).toBeCalledTimes(1);
    expect(mockFuncCalledWithMessage).toBeCalledWith(message);

    firstMemberSocket.close();
    secondMemberSocket.close();
    await models.Chat.destroy({ where: { id: 10 }, cascade: true });
    await models.ChatMember.destroy({ where: { id: [10, 20] } });
  });

  it('only members in chat should received message', async () => {
    await models.Chat.destroy({ where: { id: 100 }, cascade: true });
    await models.ChatMember.destroy({ where: { id: [100, 200, 300] } });
    await models.Chat.create(
      {
        id: 100,
        members: [
          {
            id: 100,
            userId: 100,
          },
          {
            id: 200,
            userId: 200,
          },
        ],
      },
      { include: ['members'] }
    );
    const message = {
      chatId: 100,
      content: 'Hello',
    };
    // Инициализируем сокет
    const firstMemberSocket = io(socketUrl, {
      // @ts-ignore
      auth: {
        token: jwt.sign({ id: 100 }, JWT_SECRET),
      },
    });
    const secondMemberSocket = io(socketUrl, {
      // @ts-ignore
      auth: {
        token: jwt.sign({ id: 200 }, JWT_SECRET),
      },
    });
    const thirdMemberSocket = io(socketUrl, {
      // @ts-ignore
      auth: {
        token: jwt.sign({ id: 300 }, JWT_SECRET),
      },
    });
    await Promise.all([
      waitSocketToReady(firstMemberSocket),
      waitSocketToReady(secondMemberSocket),
      waitSocketToReady(thirdMemberSocket),
    ]);

    // Вешаем обработчик за того кто не в чате
    const mockNeverCalledFn = jest.fn();
    thirdMemberSocket.on('send-message', mockNeverCalledFn);

    // Отправляем сообщение и ждём пока сервак скажет что всё ок
    await new Promise<void>((res) =>
      firstMemberSocket.emit('send-message', message, res)
    );
    // Проверяем что функция не была вызвана
    expect(mockNeverCalledFn).toBeCalledTimes(0);

    firstMemberSocket.close();
    secondMemberSocket.close();
    thirdMemberSocket.close();
    await models.Chat.destroy({ where: { id: 100 }, cascade: true });
    await models.ChatMember.destroy({ where: { id: [100, 200, 300] } });
  });

  it('reading messages', async () => {
    await models.Chat.destroy({ where: { id: 10 }, cascade: true });
    await models.ChatMember.destroy({ where: { id: [10, 20] }, cascade: true });
    await models.Chat.create(
      {
        id: 10,
        members: [
          {
            id: 10,
            userId: 1,
            messages: [
              {
                id: 1,
                chatId: 10,
                content: 'I am some message',
              },
            ],
          },
          {
            id: 20,
            userId: 2,
          },
        ],
      },
      {
        include: [
          { as: 'members', model: models.ChatMember, include: ['messages'] },
        ],
      }
    );
    // Инициализируем сокет
    const firstMemberSocket = io(socketUrl, {
      // @ts-ignore
      auth: {
        token: jwt.sign({ id: 1 }, JWT_SECRET),
      },
    });
    const secondMemberSocket = io(socketUrl, {
      // @ts-ignore
      auth: {
        token: jwt.sign({ id: 2 }, JWT_SECRET),
      },
    });

    await Promise.all([
      waitSocketToReady(firstMemberSocket),
      waitSocketToReady(secondMemberSocket),
    ]);

    const mockFuncCalledWithMessage = jest.fn();

    // Вешаем обработчик за второго участника и при получении присваиваем значения для последующей сверки
    firstMemberSocket.on('reading-message', mockFuncCalledWithMessage);

    // Отправляем сообщение и ждём пока сервак скажет что всё ок
    const response = await new Promise<void>((res) =>
      secondMemberSocket.emit('reading-message', 1, res)
    );
    expect(response).toEqual({
      status: 'successed',
    });

    expect(mockFuncCalledWithMessage).toBeCalledTimes(1);
    expect(mockFuncCalledWithMessage).toBeCalledWith(1);
    expect(
      await models.ReadedMessage.count({
        where: { messageId: 1, chatMemberId: 20 },
      })
    ).toBe(1);

    firstMemberSocket.close();
    secondMemberSocket.close();
    await models.Chat.destroy({ where: { id: 10 }, cascade: true });
    await models.ChatMember.destroy({ where: { id: [10, 20] } });
  });
});
