cp docker-compose.default.yml docker-compose.yml
cp .env.default .env
cp src/config/config.default.json src/config/config.json
cp -r .vscode.default .vscode
