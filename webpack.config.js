const path = require('path');
const NodeExternals = require('webpack-node-externals');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const webpack = require('webpack');
const NodemonPlugin = require('nodemon-webpack-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

module.exports = {
  target: 'node',
  devtool: 'inline-source-map',
  mode: 'development',
  entry: path.join(__dirname, 'src', 'index'),
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [['@babel/preset-env', { targets: { node: 'current' } }]],
          },
        },
      },
      {
        test: /\.ts?$/,
        loader: 'ts-loader',
        options: {
          configFile: path.resolve(
            process.env.NODE_ENV === 'production'
              ? './tsconfig.prod.json'
              : './tsconfig.json'
          ),
        },
      },
    ],
  },

  resolve: {
    plugins: [
      new TsconfigPathsPlugin({
        extensions: ['.ts', '.json'],
      }),
    ],
    extensions: ['.ts', '.json'],
  },
  plugins: [
    new webpack.BannerPlugin({
      raw: true,
      entryOnly: false,
      banner: `require('${
        // Is source-map-support installed as project dependency, or linked?
        require.resolve('source-map-support').indexOf(process.cwd()) === 0 // If it's resolvable from the project root, it's a project dependency.
          ? 'source-map-support/register' // It's not under the project, it's linked via lerna.
          : require.resolve('source-map-support/register')
      }');`,
    }),
    new FriendlyErrorsWebpackPlugin({
      clearConsole: process.env.NODE_ENV !== 'production',
    }),
    new NodemonPlugin({
      nodeArgs: ['--inspect=0.0.0.0:9229'],
    }),
  ],
  externals: [NodeExternals()],
};
