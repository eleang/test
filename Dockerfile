FROM node:15.3.0-alpine3.10

ARG UID=1000
ENV UID=${UID}

RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/home/chat" \
    # --ingroup "chat" \
    # --no-create-home \
    --uid "$UID" \
    chat

USER chat
RUN mkdir -p /home/chat/app
WORKDIR /home/chat/app
ENV PATH /home/chat/app/node_modules/.bin:$PATH

COPY --chown=${UID} package.json yarn.lock* ./

ARG PUPPETEER_DOWNLOAD_HOST

RUN yarn install --silent
RUN yarn cache clean --all

COPY --chown=${UID} . .

EXPOSE 4000
