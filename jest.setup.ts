/* eslint-disable import/no-extraneous-dependencies */
jest.setTimeout(20e3);
require('jest-extended');
require('jest-chain');

expect.extend({
  // TODO: Добавить возможность указывать формат в котором должна быть дата https://date-fns.org/v2.16.1/docs/isMatch
  toBeDate(date) {
    return date instanceof Date || !Number.isNaN(new Date(date))
      ? {
          message: () => `expected ${date} not to be date`,
          pass: true,
        }
      : {
          message: () => `expected ${date} to be date`,
          pass: false,
        };
  },
});
