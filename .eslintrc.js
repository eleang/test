module.exports = {
  env: {
    commonjs: true,
    es2021: true,
    node: true,
  },
  extends: [
    'airbnb-typescript/base',
    'prettier', // Uses eslint-config-prettier to disable ESLint rules from @typescript-eslint/eslint-plugin that would conflict with prettier
    'prettier/@typescript-eslint', // Uses eslint-config-prettier to disable ESLint rules from @typescript-eslint/eslint-plugin that would conflict with prettier
    // 'plugin:import/warnings',
    // "plugin:prettier/recommended" // Enables eslint-plugin-prettier and eslint-config-prettier. This will display prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: ['./tsconfig.eslint.json'],
  },
  plugins: ['@typescript-eslint', 'prettier'],
  rules: {
    'prettier/prettier': 'error',
    // Warn
    'prefer-const': 'warn',
    // Off
    radix: 'off',
    'arrow-parens': 'off',
    'no-plusplus': 'off',
    'no-continue': 'off',
    camelcase: 'off',
    'no-param-reassign': 'off',
    'import/no-dynamic-require': 'off',
    'consistent-return': 'off',
    'import/prefer-default-export': 'off',
    'func-names': 'off',
    'no-underscore-dangle': 'off',
    'no-return-await': 'off',
    'prefer-promise-reject-errors': 'off',
    'no-shadow': 'off',
    'no-confusing-arrow': 'off',
    'no-restricted-syntax': 'off',
    'no-await-in-loop': 'off',
    'no-unused-expressions': 'off',
    'array-callback-return': 'off',
    'comma-dangle': 'off',
    'global-require': 'off',
    'guard-for-in': 'off',
    'arrow-body-style': ['error', 'as-needed'],
  },
};
